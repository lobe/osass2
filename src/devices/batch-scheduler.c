/* Tests cetegorical mutual exclusion with different numbers of threads.
 * Automatic checks only catch severe problems like crashes.
 */
#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "lib/random.h" //generate random numbers
#include "devices/timer.h"

#define BUS_CAPACITY 3
#define SENDER 0
#define RECEIVER 1
#define NORMAL 0
#define HIGH 1

/*
 *	initialize task with direction and priority
 *	call o
 * */
typedef struct {
	int direction;
	int priority;
} task_t;

void batchScheduler(unsigned int num_tasks_send, unsigned int num_task_receive,
        unsigned int num_priority_send, unsigned int num_priority_receive);

void senderTask(void *);
void receiverTask(void *);
void senderPriorityTask(void *);
void receiverPriorityTask(void *);


void oneTask(task_t task);/*Task requires to use the bus and executes methods below*/
	void getSlot(task_t task); /* task tries to use slot on the bus */
	void transferData(task_t task); /* task processes data on the bus either sending or receiving based on the direction*/
	void leaveSlot(task_t task); /* task release the slot */


/* Vars */
struct lock busLock;
struct condition busEmpty;
struct condition busNotFull;
struct condition noPriorityWaiting;
int bus_utilised;
int direction;
int priorityWaiting;


/* initializes semaphores */ 
void init_bus(void){ 

    random_init((unsigned int)123456789); 
    lock_init(&busLock);
    cond_init(&busEmpty);
    cond_init(&busNotFull);
    cond_init(&noPriorityWaiting);
    direction = SENDER; // Arbitrary direction to initialise
    bus_utilised = 0;
    priorityWaiting = 0;

}

/*
 *  Creates a memory bus sub-system  with num_tasks_send + num_priority_send
 *  sending data to the accelerator and num_task_receive + num_priority_receive tasks
 *  reading data/results from the accelerator.
 *
 *  Every task is represented by its own thread. 
 *  Task requires and gets slot on bus system (1)
 *  process data and the bus (2)
 *  Leave the bus (3).
 */

void batchScheduler(unsigned int num_tasks_send, unsigned int num_tasks_receive,
        unsigned int num_priority_send, unsigned int num_priority_receive)
{
    //int count = 1;
    while (num_priority_receive > 0) {
        thread_create("Thread", PRI_DEFAULT, receiverPriorityTask, NULL);
        //msg("Count is %i\n", count++);
        num_priority_receive--;
    }
    while (num_priority_send > 0) {
        thread_create("Thread", PRI_DEFAULT, senderPriorityTask, NULL);
        //msg("Count is %i\n", count++);
        num_priority_send--;
    }
    while (num_tasks_send > 0) {
        thread_create("Thread", PRI_DEFAULT, senderTask, NULL);
        //msg("Count is %i\n", count++);
        num_tasks_send--;
    }
    while (num_tasks_receive > 0) {
        thread_create("Thread", PRI_DEFAULT, receiverTask, NULL);
        //msg("Count is %i\n", count++);
        num_tasks_receive--;
    }
}

/* Normal task,  sending data to the accelerator */
void senderTask(void *aux UNUSED){
        task_t task = {SENDER, NORMAL};
        oneTask(task);
}

/* High priority task, sending data to the accelerator */
void senderPriorityTask(void *aux UNUSED){
        task_t task = {SENDER, HIGH};
        oneTask(task);
}

/* Normal task, reading data from the accelerator */
void receiverTask(void *aux UNUSED){
        task_t task = {RECEIVER, NORMAL};
        oneTask(task);
}

/* High priority task, reading data from the accelerator */
void receiverPriorityTask(void *aux UNUSED){
        task_t task = {RECEIVER, HIGH};
        oneTask(task);
}

/* abstract task execution*/
void oneTask(task_t task) {
  getSlot(task);
  transferData(task);
  leaveSlot(task);
}


/* task tries to get slot on the bus subsystem */
void getSlot(task_t task) 
{

    lock_acquire(&busLock);

    if (task.priority == HIGH)
        priorityWaiting++;

    while ((task.direction != direction && bus_utilised != 0) || (task.direction == direction && bus_utilised >= BUS_CAPACITY) 
                || (priorityWaiting > 0 && task.priority == NORMAL)) {
        while (task.priority == NORMAL && priorityWaiting > 0)
            cond_wait(&noPriorityWaiting, &busLock);
        while (task.direction != direction && bus_utilised != 0)
            cond_wait(&busEmpty, &busLock);
        while (task.direction == direction && bus_utilised >= BUS_CAPACITY)
            cond_wait(&busNotFull, &busLock);
    }
    // At this point, the bus either has capacity and is sending in task->direction, or is empty
    direction = task.direction;
    bus_utilised++;

    // Signal that the priortiy task has got through
    if (task.priority == HIGH) {
        priorityWaiting--;
        if (priorityWaiting == 0)
            cond_signal(&noPriorityWaiting, &busLock);
    }
    lock_release(&busLock);
}

/* task processes data on the bus send/receive */
void transferData(task_t task) 
{
    //msg("Entering Transfer Task\n");
    timer_msleep(random_ulong() % 100); // Sleep randomly up to 2 seconds
    //msg("Leaving Transfer Task\n");
}

/* task releases the slot */
void leaveSlot(task_t task) 
{
    lock_acquire(&busLock);
    //printf("bus Utilisation is %i\n", *bus_utilised);    
    bus_utilised--;
    if (bus_utilised == 0)
        cond_signal(&busEmpty, &busLock);
    if (bus_utilised == BUS_CAPACITY - 1)
        cond_signal(&busNotFull, &busLock);
    lock_release(&busLock);  
}
